//
//  choumeApp.swift
//  choume
//
//  Created by Valentine on 24/06/2022.
//

import SwiftUI

@main
struct choumeApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
